package ua.atem.shop.di;

import javax.inject.Singleton;

import dagger.Component;
import ua.atem.shop.ui.TileActivity;

@Singleton
@Component(modules = DataModule.class)
public interface AppComponent {
    void injectsMainActivity(TileActivity tileActivity);
}
