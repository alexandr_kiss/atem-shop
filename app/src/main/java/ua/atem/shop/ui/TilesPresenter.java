package ua.atem.shop.ui;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import ua.atem.shop.models.BasketUnit;
import ua.atem.shop.models.Tile;
import ua.atem.shop.repository.TileRepository;

public class TilesPresenter extends MvpBasePresenter<TilesView> {
    private final TileRepository repository;
    private final TilesView tilesView;
    private final List<BasketUnit> basketUnits;

    public TilesPresenter(TileRepository repository, TilesView tilesView, List<BasketUnit> basketUnits) {
        this.repository = repository;
        this.tilesView = tilesView;
        this.basketUnits = basketUnits;
    }

    public void init() {
        repository.allTile()
                .doOnError(Throwable::printStackTrace)
                .subscribe(tilesView::showTiles);
    }

    public void onClickItem(Tile tile) {
        basketUnits.add(new BasketUnit(tile));
        tilesView.showToastAddBasked("Добавлено: "+tile.getName());
    }
}