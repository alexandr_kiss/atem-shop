package ua.atem.shop.ui;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ua.atem.shop.R;
import ua.atem.shop.models.Tile;

public class TileHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tile_item_name) TextView name;
    @BindView(R.id.tile_item_price) TextView price;
    @BindView(R.id.tile_item_image) ImageView imageView;

    private final TilesPresenter presenter;
    private Tile tile;

    public TileHolder(@NonNull View itemView, TilesPresenter presenter) {
        super(itemView);
        this.presenter = presenter;
        ButterKnife.bind(this, itemView);
    }


    public void bind(Tile tile) {
        this.tile = tile;
        name.setText(tile.getName());
        price.setText(String.valueOf(tile.getPrice()));
        Picasso.get().load(tile.getImage()).into(imageView);
    }

    @OnClick(R.id.tile_list_item) void onClickItem() {
        presenter.onClickItem(tile);
    }
}
