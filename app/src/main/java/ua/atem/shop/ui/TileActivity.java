package ua.atem.shop.ui;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.atem.shop.AppTile;
import ua.atem.shop.R;
import ua.atem.shop.models.BasketUnit;
import ua.atem.shop.models.Tile;
import ua.atem.shop.repository.BasketStorage;
import ua.atem.shop.repository.TileRepository;
import ua.atem.shop.ui.basket.BasketActivity;

public class TileActivity extends MvpActivity<TilesView,TilesPresenter> implements TilesView {
    @Inject
    TileRepository repository;


    @BindView(R.id.list)
    RecyclerView tileListView;


    private TilesPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tiles_list_activity);

        ButterKnife.bind(this);
        AppTile.getComponent().injectsMainActivity(this);

        presenter = createPresenter();
        presenter.init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.basket, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.basket) {
            openBasketActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openBasketActivity() {
        Intent intent = new Intent(this, BasketActivity.class);
        startActivity(intent);
    }

    @NonNull
    @Override
    public TilesPresenter createPresenter() {
        return new TilesPresenter(repository, this, BasketStorage.getInstance().getBasketUnits());
    }

    @Override
    public void showTiles(List<Tile> tiles) {
        TileListAdapter adapter = new TileListAdapter(tiles, presenter);
        tileListView.setAdapter(adapter);
    }

    @Override
    public void showToastAddBasked(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setCountBasket(int count) {

    }

    @Override
    public void showError() {
        // TODO: need to write showError() method implementation
    }
}