package ua.atem.shop.ui.basket;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import ua.atem.shop.models.BasketUnit;

public interface BasketView extends MvpView {
    void showBasket(List<BasketUnit> tiles);
}
