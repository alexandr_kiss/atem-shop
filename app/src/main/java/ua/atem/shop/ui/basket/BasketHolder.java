package ua.atem.shop.ui.basket;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.atem.shop.R;
import ua.atem.shop.models.BasketUnit;
import ua.atem.shop.repository.BasketStorage;

public class BasketHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.basket_item_name) TextView textView;
    @BindView(R.id.basket_item_count) EditText countView;
    @BindView(R.id.basket_item_summ) TextView sumView;

    public BasketHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @SuppressLint("SetTextI18n")
    public void bind(BasketUnit basketUnit) {
        textView.setText(basketUnit.getName());
        countView.setText(String.valueOf(basketUnit.getCount()));
        int count = basketUnit.getCount();
        Double price = basketUnit.getPrice();
        sumView.setText((count*price)+" грн.");
    }
}
