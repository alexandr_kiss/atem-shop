package ua.atem.shop.ui;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import ua.atem.shop.models.Tile;

public interface TilesView extends MvpView {
    void showTiles(List<Tile> tiles);
    void openBasketActivity();
    void showError();
    void setCountBasket(int count);
    void showToastAddBasked(String text);
}
