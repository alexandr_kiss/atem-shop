package ua.atem.shop.ui.basket;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ua.atem.shop.R;
import ua.atem.shop.models.BasketUnit;
import ua.atem.shop.repository.BasketStorage;

public class BasketAdapter extends RecyclerView.Adapter<BasketHolder> {
    @NonNull
    @Override
    public BasketHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BasketHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.basket_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BasketHolder holder, int position) {
        BasketUnit basketUnit = BasketStorage.getInstance().getBasketUnits().get(position);
        holder.bind(basketUnit);
    }

    @Override
    public int getItemCount() {
        return BasketStorage.getInstance().getBasketUnits().size();
    }
}
