package ua.atem.shop.ui.basket;

import android.os.Bundle;
import android.util.Log;
import android.widget.ScrollView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ua.atem.shop.R;
import ua.atem.shop.models.BasketUnit;
import ua.atem.shop.repository.BasketStorage;
import ua.atem.shop.ui.TileListAdapter;

public class BasketActivity extends MvpActivity<BasketView,BasketPresenter> implements BasketView{

    private BasketPresenter presenter;

    @BindView(R.id.basket_all_item)
    RecyclerView basketListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.basket_activity);
        ButterKnife.bind(this);

        Log.i("Корзина", BasketStorage.getInstance().getBasketUnits().toString());
        presenter = createPresenter();

        showBasket(BasketStorage.getInstance().getBasketUnits());
    }

    @NonNull
    @Override
    public BasketPresenter createPresenter() {
        return new BasketPresenter();
    }

    @Override
    public void showBasket(List<BasketUnit> tiles) {
        if (tiles.size()!=0) {
            LinearLayoutManager verticalLayoutManager
                    = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            basketListView.setLayoutManager(verticalLayoutManager);
            BasketAdapter adapter = new BasketAdapter();
            basketListView.setAdapter(adapter);
        }
    }
}
