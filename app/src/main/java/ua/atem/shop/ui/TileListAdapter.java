package ua.atem.shop.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ua.atem.shop.R;
import ua.atem.shop.models.Tile;

public class TileListAdapter extends RecyclerView.Adapter<TileHolder> {
    private final List<Tile> tileList;
    private final TilesPresenter presenter;

    public TileListAdapter(List<Tile> tileList, TilesPresenter presenter) {
        this.tileList = tileList;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public TileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new TileHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tile_activity, parent, false),presenter);
    }

    @Override
    public void onBindViewHolder(@NonNull TileHolder holder, int position) {
        Tile tile = tileList.get(position);
        holder.bind(tile);
    }

    @Override
    public int getItemCount() {
        return tileList.size();
    }
}
