package ua.atem.shop.repository;

import android.util.Log;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import ua.atem.shop.api.APIService;
import ua.atem.shop.models.Tile;

public class TileRepositoryImpl implements TileRepository {
    private final APIService apiService;

    public TileRepositoryImpl(APIService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Single<List<Tile>> allTile() {
        Log.i("TileRepositoryImpl", apiService.toString());
        return apiService.loadListTiles()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace);
    }
}