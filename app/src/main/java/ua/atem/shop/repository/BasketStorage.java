package ua.atem.shop.repository;

import java.util.ArrayList;
import java.util.List;

import ua.atem.shop.models.BasketUnit;

public class BasketStorage {
    private static BasketStorage mInstance;
    private List<BasketUnit> basketUnits;

    private BasketStorage() {
        basketUnits = new ArrayList<>();
    }

    public static BasketStorage getInstance() {
        if (mInstance == null) {
            mInstance = new BasketStorage();
        }
        return mInstance;
    }

    public List<BasketUnit> getBasketUnits() {
        return basketUnits;
    }
}
