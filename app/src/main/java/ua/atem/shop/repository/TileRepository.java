package ua.atem.shop.repository;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import ua.atem.shop.models.Tile;

public interface TileRepository {
    Single<List<Tile>> allTile();
}
