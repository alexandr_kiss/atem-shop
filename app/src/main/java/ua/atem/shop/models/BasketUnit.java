package ua.atem.shop.models;

public class BasketUnit extends Tile {
    int count;

    public BasketUnit(Tile tile) {
        super(tile);
        count++;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "BasketUnit{" +
                "id=" + super.getId() +
                ", name='" + super.getName() + '\'' +
                ", price=" + super.getPrice() +
                ", image='" + super.getImage() + '\'' +
                "count=" + count +
                '}';
    }
}