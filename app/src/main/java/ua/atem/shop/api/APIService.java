package ua.atem.shop.api;

import java.util.List;

import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import ua.atem.shop.models.Tile;

public interface APIService {
    @GET("posts")
    Single<List<Tile>> loadListTiles();
}
