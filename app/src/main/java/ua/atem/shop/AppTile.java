package ua.atem.shop;

import android.app.Application;

import ua.atem.shop.di.AppComponent;
import ua.atem.shop.di.DaggerAppComponent;

public class AppTile extends Application {
    private static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.create();
    }

    public static AppComponent getComponent() {
        return component;
    }
}
